//
//  WeatherViewTable.swift
//  Weather
//
//  Created by Stefan Ruzic on 22.8.17..
//  Copyright © 2017. Stefan Ruzic. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation


class WeatherViewTable: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var daysArray: Array<Any> = []
    var grad: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = UIColor.clear
//        self.tableView.allowsSelection = false
        self.loadData()
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(loadData), for: UIControlEvents.valueChanged)
        self.tableView.refreshControl = refreshControl
        self.tableView.isUserInteractionEnabled = false
    }
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.daysArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WeatherCell
        cell.backgroundColor = UIColor.clear
        let singleDay = SingleDay(dictionary: self.daysArray[indexPath.row] as! [String: Any])
        tm()
        var temperatureInt = singleDay.temperatureMax
        temperatureInt = convertToCelsius(fahrenheit: temperatureInt)
        let temperatureString = String(describing: temperatureInt)
        var temperatureMinInt = singleDay.temperatureMin
        temperatureMinInt = convertToCelsius(fahrenheit: temperatureMinInt)
        let temperatureMinString = String(describing: temperatureMinInt)
        
        cell.temperature.text = "\(temperatureString)℃"
        cell.temperature.textColor = UIColor.cyan
        cell.temperatureMin.text = "\(temperatureMinString)℃"
        cell.temperatureMin.textColor = UIColor.cyan
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let dateString = dateFormatter.string(from: singleDay.date!)
        cell.date.text = dateString
        cell.date.textColor = UIColor.white
        cell.icon?.image = UIImage(named: singleDay.icon)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("\(indexPath.row)")
        
        UserDefaults.standard.removeObject(forKey: "defaultCity")
        
    }
    func convertToCelsius(fahrenheit: Int) -> Int {
        return Int(5.0 / 9.0 * (Double(fahrenheit) - 32.0))
    }
    @objc func loadData(){
        CLGeocoder().geocodeAddressString(self.grad!) { (placemarks:[CLPlacemark]?, error:Error?) in
            if let location = placemarks?.first?.location{
                ForecastService.sharedInstance.getForecast(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude) { (niz) in
                    self.daysArray = niz!
                    self.tableView.reloadData()
                    self.tableView.refreshControl?.endRefreshing()
                }
            }
        }
    }
}

