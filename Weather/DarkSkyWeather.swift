//
//  DarkSkyWeather.swift
//  Weather
//
//  Created by Stefan Ruzic on 9.8.17..
//  Copyright © 2017. Stefan Ruzic. All rights reserved.
//

import Foundation

class DarkSkyWeather {
    
    let latitude: Int?
    let longitude: Int?
    let city: String?
    let currently: CurrentWeather?
    let daily: DailyWeather?
    
    struct DarkSkyKey {
        static let latitude = "latitude"
        static let longitude = "longitude"
        static let city = "timezone"
        static let currently = "currently"
        static let daily = "daily"
    }
    init(dictionary: [String : Any]) {
        latitude = dictionary[DarkSkyKey.latitude] as? Int
        longitude = dictionary[DarkSkyKey.longitude] as? Int
        city = dictionary[DarkSkyKey.city] as? String
        currently = CurrentWeather(weatherDictionary: dictionary[DarkSkyKey.currently] as! [String : Any])
        daily = DailyWeather(dictionary: dictionary["daily"] as! [String : Any])
    }
}
