//
//  FirstCityViewController.swift
//  Weather
//
//  Created by Stefan Ruzic on 27.9.17..
//  Copyright © 2017. Stefan Ruzic. All rights reserved.
//

import UIKit
import CoreLocation

class FirstCityViewController: UIViewController, UISearchBarDelegate {
    
    
    @IBOutlet weak var firstSearchBar: UISearchBar!

    
    
    var city: CurrentWeatherViewController?
    var ForecastService: ForecastService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstSearchBar.showsCancelButton = false
        firstSearchBar.delegate = self
        self.firstSearchBar.delegate = self
        firstSearchBar.barTintColor = .white
        let textFieldInsideSearchBar = firstSearchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = .white
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        firstSearchBar.setShowsCancelButton(true, animated: true)    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        firstSearchBar.text = nil
        firstSearchBar.text = ""
        firstSearchBar.endEditing(false)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        firstSearchBar.showsCancelButton = false
    }
    //MARK: - Searchbar delegate

    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
    city?.currentCity = searchText
        
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        searchBar.text = ""
   self.firstSearchBar.resignFirstResponder()
    
    }
}




