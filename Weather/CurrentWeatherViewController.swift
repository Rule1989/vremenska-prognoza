//
//  CurrentWeatherViewController.swift
//  Weather
//
//  Created by Stefan Ruzic on 5.8.17..
//  Copyright © 2017. Stefan Ruzic. All rights reserved.
//

import UIKit
import CoreLocation


class CurrentWeatherViewController: UIViewController, UISearchBarDelegate {
    @IBOutlet weak var cityTextLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var weatherInfoContainer: UIView!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var navigationButton: UIButton!
    
    var currentCity: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let currentCity1 = UserDefaults.standard.value(forKey: "defaultCity") {
            currentCity = currentCity1 as? String
            self.loadData()
        }else{
            print("Otvori popup za biranje grada")
        }
         goButton.layer.borderWidth = 1
         goButton.layer.cornerRadius = 5
        goButton.layer.borderColor = UIColor.cyan.cgColor
        
        currentLocationButton.layer.cornerRadius = 5
        currentLocationButton.layer.borderWidth = 1
        currentLocationButton.layer.borderColor = UIColor.cyan.cgColor
        
        navigationButton.layer.cornerRadius = 5
        navigationButton.layer.borderWidth = 1
        navigationButton.layer.borderColor = UIColor.cyan.cgColor
        
        searchBar.showsCancelButton = false
        searchBar.delegate = self
        self.searchBar.delegate = self
        
        let cornerRadiusValue = weatherInfoContainer.frame.size.width / 2
        weatherInfoContainer.layer.cornerRadius = cornerRadiusValue
        weatherInfoContainer.layer.borderColor = UIColor.cyan.cgColor
        weatherInfoContainer.layer.borderWidth = 1
        
        LocationManager.shared.startUpdatingLocation()
        
        searchBar.barTintColor = .white
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = .white

        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)

    }
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.text = ""
        searchBar.endEditing(false)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    func loadData(){
        CLGeocoder().geocodeAddressString(self.currentCity!) { (placemarks:[CLPlacemark]?, error:Error?) in
            if let location = placemarks?.first?.location{
                ForecastService.sharedInstance.getCurrentWeather(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude) {  (darkSkyWeather) in
                    if let currentWeather = darkSkyWeather {
                        DispatchQueue.main.async {
                            if let temperature = currentWeather.currently?.temperature {
                                self.temperatureLabel.text = "\(temperature)"
                            } else {
                                self.temperatureLabel.text = "-"
                            }
                            if let city = currentWeather.city{
                                let locationComponents = city.components(separatedBy: "/")
                                self.cityTextLabel.text = locationComponents[1]
                            }
                            if let backgroundImage = currentWeather.currently?.summary{
                                self.backgroundImageView.image = UIImage(named: backgroundImage)
                            }
                            if let iconImage = currentWeather.currently?.icon {
                                self.iconImageView.image = UIImage(named: iconImage)
                            }
                        }
                    }
                }
            }
        }
    }
    //MARK: - Searchbar delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        self.currentCity = searchText

    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        searchBar.text = ""
        self.loadData()
        self.searchBar.resignFirstResponder()


    }
    
    @IBAction func goToForecastAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToForecastVC", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToForecastVC" {
            let forecastVC = segue.destination as! WeatherViewTable
            forecastVC.grad = self.currentCity
        }
    }
    @IBAction func saveLocation(_ sender: Any) {
        
        UserDefaults.standard.set(self.currentCity, forKey: "defaultCity")
        print(currentCity!)
        

    }
    @IBAction func dajLokaciju(_ sender: UIButton) {
        let lokacija = LocationManager.shared.getCurrentLocation()

        ForecastService.sharedInstance.getCurrentWeather(latitude: lokacija.coordinate.latitude, longitude: lokacija.coordinate.longitude) {  (darkSkyWeather) in
            if let currentWeather = darkSkyWeather {
                
                DispatchQueue.main.async {
                    let temperature = currentWeather.currently?.temperature
                    
                    let city = currentWeather.city
                    let locationComponents = city!.components(separatedBy: "/")
                    self.currentCity = locationComponents[1]
                    let backgroundImage = currentWeather.currently?.summary
                    let iconImage = currentWeather.currently?.icon
                    self.setupUIwith(temperature: temperature!, city: locationComponents[1], backgrounImage: backgroundImage!, iconImage: iconImage!)
                }
            }
        }
    }
    func setupUIwith(temperature: Int, city: String, backgrounImage: String, iconImage: String ) {
        self.temperatureLabel.text = "\(temperature)"
        self.cityTextLabel.text = city
        self.backgroundImageView.image = UIImage(named: backgrounImage)
        self.iconImageView.image = UIImage(named: iconImage)
    }
}

