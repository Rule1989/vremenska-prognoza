//
//  SingleDay.swift
//  Weather
//
//  Created by Stefan Ruzic on 22.8.17..
//  Copyright © 2017. Stefan Ruzic. All rights reserved.
//

import Foundation


class SingleDay {
    
    let icon : String
    let temperatureMax: Int
    let temperatureMin: Int
    let date: Date?
    
    init(dictionary: [String : Any]) {
        icon = (dictionary["icon"] as? String)!
        let maximalnaTemperatura: Float = dictionary["temperatureMax"] as! Float
        temperatureMax = Int(maximalnaTemperatura)
        let minimalnaTemperatur: Float = dictionary["temperatureMin"] as! Float
        temperatureMin = Int(minimalnaTemperatur)
        date = Date(timeIntervalSince1970: (dictionary["temperatureMaxTime"] as? Double)!)
    }
}
