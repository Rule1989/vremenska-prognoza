//
//  CurrentWeather.swift
//  Weather
//
//  Created by Stefan Ruzic on 2.8.17..
//  Copyright © 2017. Stefan Ruzic. All rights reserved.
//

import Foundation

class CurrentWeather {
    
    var temperature: Int?
    let humidity: Int?
    let precipProbability: Int?
    let summary: String?
    let icon: String?
    let timezone: String?
    
    struct WeatherKeys {
        
        static let temperature = "apparentTemperature"
        static let humidity = "humidity"
        static let precipProbability = "precipProbability"
        static let summary = "summary"
        static let icon = "icon"
        static let timezone = "timezone"
    }
    init(weatherDictionary: [String : Any]) {
        func convertToCelsius(fahrenheit: Float) -> Int {
            return Int(5.0 / 9.0 * (Double(fahrenheit) - 32.0))
        }
        let fahrenheit = weatherDictionary[WeatherKeys.temperature] as? Float
        temperature = convertToCelsius (fahrenheit: fahrenheit!)
        if let humidityDoble = weatherDictionary[WeatherKeys.humidity] as? Double {
            humidity = Int(humidityDoble * 100)
        } else {
            humidity = nil
        }
        
        if let precipDoble = weatherDictionary[WeatherKeys.precipProbability] as? Double {
            precipProbability = Int(precipDoble * 100)
        } else {
            precipProbability = nil
        }
        summary = weatherDictionary[WeatherKeys.summary] as? String
        icon = weatherDictionary[WeatherKeys.icon] as? String
        timezone = weatherDictionary[WeatherKeys.timezone] as? String
    }
}
