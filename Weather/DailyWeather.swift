//
//  DailyWeather.swift
//  Weather
//
//  Created by Stefan Ruzic on 16.8.17..
//  Copyright © 2017. Stefan Ruzic. All rights reserved.
//

import Foundation

class DailyWeather  {
    
    let summary : String?
    let icon : String?
    let temperatureMin : Int?
    let temperatureMax : Int?
    
    struct DailyKey {
        static let summary = "summary"
        static let icon = "icon"
        static let temperatureMin = "temperatureMin"
        static let temperatureMax = "temperatureMax"
    }
    init(dictionary: [String : Any]) {
        summary = dictionary[DailyKey.summary] as? String
        icon = dictionary[DailyKey.icon] as? String
        temperatureMax = dictionary[DailyKey.temperatureMax] as? Int
        temperatureMin = dictionary[DailyKey.temperatureMin] as? Int
    }
}
