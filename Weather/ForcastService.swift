 //
//  ForcastService.swift
//  Weather
//
//  Created by Stefan Ruzic on 2.8.17..
//  Copyright © 2017. Stefan Ruzic. All rights reserved.
//

import Foundation
import Alamofire

class ForecastService {
    //URL https://api.darksky.net/forecast/f01a30571dbbde2e00837460d17dd1ad/37.8267,-122.4233
    
    let forecastBaseURL: URL?
    let APIKey = "f01a30571dbbde2e00837460d17dd1ad"
    let baseUrl = "https://api.darksky.net/forecast/"
    static let sharedInstance = ForecastService()

    init() {
        forecastBaseURL = URL(string: "\(baseUrl)\(self.APIKey)")
    }
    func getCurrentWeather(latitude: Double,longitude: Double, completion: @escaping (DarkSkyWeather?) -> Void) {
        if let forcasteURL = URL(string: "\(String(describing: forecastBaseURL!))/\(latitude),\(longitude)") {
            Alamofire.request(forcasteURL).responseJSON(completionHandler: { (response) in
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentWeather = DarkSkyWeather(dictionary: jsonDictionary)
                    completion(currentWeather)
                } else {
                    completion(nil)
                }
            })
        }
    }
    func getForecast(latitude: Double,longitude: Double, completion: @escaping (Array<Any>?) -> Void) {
        if let forcasteURL = URL(string: "\(String(describing: forecastBaseURL!))/\(latitude),\(longitude)") {
            Alamofire.request(forcasteURL).responseJSON(completionHandler: { (response) in
                if let jsonDictionary = response.result.value as? [String : Any] {
                    if let daily = jsonDictionary["daily"] as? [String : Any]{
                        if let data1 = daily["data"] as? Array<Any>{
                            completion (data1)
                        }
                    }
                }
            })
        }
    }
}
